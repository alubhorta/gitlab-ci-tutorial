FROM node:10-alpine

RUN apk update \
  && apk upgrade

RUN mkdir app 
WORKDIR /app
COPY . .

RUN npm install serve

CMD [ "./node_modules/.bin/serve", ".", "-l", "4200" ]
