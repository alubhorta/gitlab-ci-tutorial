# Gitlab CI/CD + Registry tutorial

## CI/CD config

The ```.gitlab-ci.yml``` file holds the config for CI/CD.

## Gitlab Registry - Store Docker images in your repo

Be sure you login to your particular registry. Further documentation on [Gitlab Registry](https://gitlab.com/help/user/packages/container_registry/index).

```bash
sudo docker login registry.example.com

# or for e.g. gitlab registry
sudo docker login registry.gitlab.com/alubhorta
```

Build and push your docker image.

```bash
sudo docker build -t registry.example.com/group/project/image-name:tag path/to/Dockerfile

sudo docker push registry.example.com/group/project/image
```

### Usage

Building or Pulling docker image.

```bash
# building image locally
sudo docker build -t registry.gitlab.com/alubhorta/gitlab-ci-tutorial/node:10-alpine .

# or pulling from registry
sudo docker pull registry.gitlab.com/alubhorta/gitlab-ci-tutorial/node:10-alpine
```

Running docker image.

```bash
# running image
sudo docker run --rm -p 4200:4200 registry.gitlab.com/alubhorta/gitlab-ci-tutorial/node:10-alpine
```